package br.com.proway.consultoriomedico.model;

public class Paciente extends Pessoa{

    public boolean possuiConvenio;
    public String motivoConsulta;

    public Paciente(int id, String nome, String telefone, Endereco endereco, boolean possuiConvenio, String motivoConsulta) {
        super(id, nome, telefone, endereco);
        this.possuiConvenio = possuiConvenio;
        this.motivoConsulta = motivoConsulta;
    }

    public boolean isPossuiConvenio() {
        return possuiConvenio;
    }

    public void setPossuiConvenio(boolean possuiConvenio) {
        this.possuiConvenio = possuiConvenio;
    }

    public String getMotivoConsulta() {
        return motivoConsulta;
    }

    public void setMotivoConsulta(String motivoConsulta) {
        this.motivoConsulta = motivoConsulta;
    }
}
