package br.com.proway.consultoriomedico.model;

import br.com.proway.consultoriomedico.utils.Especialidade;

public class Medico  extends Pessoa implements Exame {

    private String crm;
    private boolean status;

    public Medico(int id, String nome, String telefone, Endereco endereco) {
        super(id, nome, telefone, endereco);
    }

    public String getCrm() {
        return crm;
    }

    public void setCrm(String crm) {
        this.crm = crm;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String emiteAtestado(Medico medico, Paciente paciente, Especialidade especialidade){
        String mensagem = "\nAtestado: \nAtesto que o senhor " + paciente.getNome() + " é um preguiçoso que não quer ir trabalhar.\n\nAssinado,\n      Dr. " + medico.getNome() + ", especialista em " + especialidade.getNome() + ".\n";
        return mensagem;
    }

    @Override
    public String emiteLaudo(boolean resultado) {
        String mensagem;
        if(true) {
            mensagem = "Laudo: O prognóstico é ruim. Você tem 5 minutos de vida.";
        } else {
            mensagem = "Laudo: Nenhum problema encontrado no exame.";
        }

        return mensagem;
    }
}