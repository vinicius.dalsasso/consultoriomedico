package br.com.proway.consultoriomedico.model;

public interface Exame {
    public String emiteLaudo(boolean resultado);
}
