package br.com.proway.consultoriomedico;

import br.com.proway.consultoriomedico.model.Endereco;
import br.com.proway.consultoriomedico.model.Medico;
import br.com.proway.consultoriomedico.model.Paciente;
import br.com.proway.consultoriomedico.utils.Especialidade;

import javax.swing.*;

public class Main {

    public static void main (String[] args) {

        // Criacoes de enderecos manual
        Endereco enderecoMedico = new Endereco(1, "Rua Tapir", "Pato Branco", "PR", "85502-010","Brasil");
        Endereco endereco1 = new Endereco(2, "Rua XYZ", "São Paulo", "SP", "00010-010","Brasil");
        Endereco endereco2 = new Endereco(3, "Rua dos Karatecas", "Vermelhândia", "PR", "85500-010","Brasil");

        // Definindo um enum a ser usado
        Especialidade especialidadeEnum = Especialidade.CARDIOLOGIA;

        // Criacao de objetos manual
        Medico medico = new Medico(1, "", "(46) 9999-0000", enderecoMedico);
        Medico medico2 = new Medico(2, "Estranho", "(46) 9999-0001", enderecoMedico);

        Paciente paciente = new Paciente(1, "", "(11) 1234-5678", endereco1, false, "");
        Paciente paciente2 = new Paciente(2, "Adilson Polloskki", "(45) 9999-1234", endereco2, true, "Dor no apêndice");


        medico2.emiteLaudo(true);


        while(true) {
            String texto = "        Sistema de Consulta\n";

            String[] opcoes = {"Cadastrar Paciente", "Cadastrar Medico", "Emitir Atestado", "Emitir Laudo", "Sair"};

            int opcao = JOptionPane.showOptionDialog(null, texto, "", JOptionPane.DEFAULT_OPTION, JOptionPane.INFORMATION_MESSAGE, null, opcoes, opcoes[0]);

            if (opcao == 0) {
                paciente.setNome(JOptionPane.showInputDialog("Informe o nome do Paciente"));
                paciente.setMotivoConsulta(JOptionPane.showInputDialog("Informe o motivo da consulta"));
                JOptionPane.showMessageDialog(null, "Paciente : "+paciente.getNome()+" cadastrado com sucesso");

            } else if (opcao == 1) {
                medico.setNome(JOptionPane.showInputDialog("Informe o nome do Medico"));
                medico.setCrm(JOptionPane.showInputDialog("Informe o CRM do medico"));
                JOptionPane.showMessageDialog(null, "Medico : "+medico.getNome()+" cadastrado com sucesso");

            } else if (opcao == 2) {

                // verifica se ja foram passados os nomes do paciente e medico
                if(medico.getNome() != "" && paciente.getNome() != "") {
                    String mensagem = medico.emiteAtestado(medico, paciente, especialidadeEnum);

                    JOptionPane.showMessageDialog(null, mensagem);
                } else {
                    JOptionPane.showMessageDialog(null, "Por favor, informe o paciente E o médico responsável antes de tentar a emissão de um atestado");

                }
            } else if(opcao == 3) {
                // verifica se ja foram passados os nomes do paciente e medico
                if(medico.getNome() != "" && paciente.getNome() != "") {
                    String mensagem = medico.emiteLaudo(true);

                    JOptionPane.showMessageDialog(null, mensagem);
                } else {
                    JOptionPane.showMessageDialog(null, "Por favor, informe o paciente E o médico responsável antes de tentar a emissão de um laudo");

                }
            }else{
                break;
            }

        }
    }
}
