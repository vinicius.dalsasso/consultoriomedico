package br.com.proway.consultoriomedico.utils;

public enum Especialidade {
    CLINICOGERAL(1, "Clínica Geral"),
    ORTOPEDIA(2, "Ortopedia"),
    TRAUMATOLOGIA(3, "Traumatologia"),
    CARDIOLOGIA(4, "Cardiologia");

    private final int id;
    private final String nome;

    Especialidade(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public String getNome() {
        return nome;
    }
}
