# ConsultorioMedico

Projeto para prova 1 da sprint 3 do curso Idevs - Proway

## Paciente e Médico

Recebe informações do Paciente e do Médico.
Utilizam herança da classe abstrata pessoa.

## Emissão de Atestado
Exibe uma emissão de "atestado" caso seja informado um paciente e médico. Função da classe Médico.

## Emissão de laudo

Exibe mensagem sobre o prognóstico conforme o que for setado na função. Função contida na interface Exame.



